const express = require('express');
const app = express();
const expressGraphQL = require('express-graphql');
const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLInputObjectType
} = require('graphql');

const authors = require('../authors');
const books = require('../books');

//----------------SCHEMAS-------------------------------------

const BookType = new GraphQLObjectType({
  name: 'Book',
  description: 'Represent books',
  fields: () => ({
    id: { type: GraphQLNonNull(GraphQLInt) },
    title: { type: GraphQLNonNull(GraphQLString) },
    isbn: { type: GraphQLNonNull(GraphQLString) },
    pageCount: { type: GraphQLNonNull(GraphQLInt) },
    publishedDate: { type: GraphQLNonNull(PublishedDateType) },
    thumbnailUrl: { type: GraphQLNonNull(GraphQLString) },
    shortDescription: { type: GraphQLNonNull(GraphQLString) },
    longDescription: { type: GraphQLNonNull(GraphQLString) },
    status: { type: GraphQLNonNull(GraphQLString) },
    categories: {
      type: new GraphQLList(GraphQLString),
      resolve(parent, args) {
        const c = [];
        const bookList = books.find(book => book.id === parent.id);
        bookList.categories.filter(category => {
          c.push(category)
        });
        return c;
      }
    },
    authorID: { type: GraphQLNonNull(GraphQLInt) },
    author: {
      type: AuthorType,
      resolve: (book) => {
        return authors.find(author => author.id === book.authorID)
      }
    }
  })
});

const AuthorType = new GraphQLObjectType({
  name: 'Author',
  description: 'Represent authors',
  fields: () => ({
    id: { type: GraphQLNonNull(GraphQLInt) },
    name: { type: GraphQLNonNull(AuthorNameType) },
    books: {
      type: new GraphQLList(BookType),
      resolve: (author) => {
        return books.filter(book => book.authorID === author.id)
      }
    }
  })
});

//----------------SUBSQUEMAS-------------------------------------

const AuthorNameType = new GraphQLObjectType({
  name: 'Name',
  description: 'Represent Category of Books',
  fields: {
    first: { type: GraphQLNonNull(GraphQLString) },
    last: { type: GraphQLNonNull(GraphQLString) }
  }
});

const PublishedDateType = new GraphQLObjectType({
  name: 'PublishedDate',
  description: 'Represent PublishedDate of Books',
  fields: {
    date: { type: GraphQLNonNull(GraphQLString) }
  }
});

//----------------INPUT SUBSQUEMAS-----------------------------

const AuthorNameInputType = new GraphQLInputObjectType({
  name: 'NameInput',
  description: 'Represent Category of Books',
  fields: {
    first: { type: GraphQLNonNull(GraphQLString) },
    last: { type: GraphQLNonNull(GraphQLString) }
  }
});

const PublishedDateInputType = new GraphQLInputObjectType({
  name: 'PublishedDateInput',
  description: 'Represent PublishedDate of Books',
  fields: {
    date: {
      type: GraphQLString,
    }
  }
});

//----------------ROOTS-----------------------------------------

const RootQueryType = new GraphQLObjectType({
  name: 'Query',
  description: 'Root Query',
  fields: () => ({
    books: {
      type: new GraphQLList(BookType),
      description: 'List of All Books',
      resolve: () => books
    },
    authors: {
      type: new GraphQLList(AuthorType),
      description: 'List of All Authors',
      resolve: () => authors
    },
    book: { // para buscar un libro por id
      type: BookType,
      description: 'Particular Book',
      args: {
        id: { type: GraphQLInt }
      },
      resolve: (parent, args) => books.find(book => book.id === args.id)
    },
    author: { // para buscar un autor por id
      type: AuthorType,
      description: 'Particular Author',
      args: {
        id: { type: GraphQLInt }
      },
      resolve: (parent, args) => authors.find(author => author.id === args.id)
    },
  }),
});

const RootMutationType = new GraphQLObjectType({
  name: 'Mutation',
  description: 'Root Mutation',
  fields: () => ({
    addBook: {
      type: BookType,
      description: 'Add a book',
      args: {
        title: { type: GraphQLNonNull(GraphQLString) },
        isbn: { type: GraphQLString },
        pageCount: { type: GraphQLInt },
        publishedDate: { type: PublishedDateInputType },
        thumbnailUrl: { type: GraphQLString },
        shortDescription: { type: GraphQLString },
        longDescription: { type: GraphQLString },
        status: { type: GraphQLString },
        categories: {
          type: new GraphQLList(GraphQLString),
          resolve(parent, args) {
            const c = [];
            const bookList = books.find(book => book.id === parent.id);
            bookList.categories.filter(category => {
              c.push(category)
            });
            return c;
          }
        },
        authorID: { type: GraphQLNonNull(GraphQLInt) },
      },
      resolve: (parent, args) => {
        const book = {
          id: books.length + 1,
          title: args.title,
          isbn: args.isbn,
          pageCount: args.pageCount,
          publishedDate: {date: new Date().toLocaleTimeString()},
          thumbnailUrl: args.thumbnailUrl,
          shortDescription: args.shortDescription,
          longDescription: args.longDescription,
          status: args.status,
          categories: args.categories,
          authorID: args.authorID
        };
        books.push(book);
        return book
      }
    },
    addAuthor: {
      type: AuthorType,
      description: 'Add a author',
      args: {
        name: { type: GraphQLNonNull(AuthorNameInputType) },
      },
      resolve: (parent, args) => {
        const author = {
          id: authors.length + 1,
          name: {
            first: args.name.first,
            last: args.name.last
          },
        };
        authors.push(author);
        return author
      }
    }
  })
});

const schema = new GraphQLSchema({
  query: RootQueryType,
  mutation: RootMutationType
});

app.use('/graphql', expressGraphQL({
  schema: schema,
  graphiql: true
}));

const port = 3001;

app.listen(port, ()=>{
  console.log(`Server running aT port ${port}`);
});
